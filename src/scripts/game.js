var canvas = document.getElementById("game").getContext("2d");
canvas.canvas.width = canvas.canvas.clientWidth;
canvas.canvas.height = canvas.canvas.clientHeight;
var width = canvas.canvas.width;
var height = canvas.canvas.height;
var interval_main_loop = setInterval(gLoop, 16)

var stage1 = {
	stage: "stage1",
	terrain:[
			{x: 50, y: 50, w: 100, h:150, t:"concrete"}
			],
	characters:[
			{x: 50, y: 50, w: 100, h:150, t:"concrete"}
			]
}

var world = stage1;

function gLoop()
{
	canvas.fillStyle = "#ffffff"
	canvas.fillRect(0,0,width, height);
	/* ^ clear to white */
	drawBackground();
	drawTerrain();
	drawCharacters();
}

//////////

function drawBackground()
{
	var _img = new Image();
	_img.src = "images/" + world.stage + "/background.png";
	canvas.drawImage(_img, 0, 0, width, height);
}

function drawTerrain()
{
	for(var i = 0; i < world.terrain.length; i++)
	{
		var _img = new Image();
		_img.src = "images/" + world.stage + "/" + world.terrain[i].t + ".png";
		canvas.drawImage(_img, world.terrain[i].x, world.terrain[i].y, world.terrain[i].w, world.terrain[i].h)
	}
}

function drawCharacters()
{
	for(var i = 0; i < world.characters.length; i++)
	{
		var _img = new Image();
		_img.src = "images/characters/" + world.characters[i].t + ".png";
		canvas.drawImage(_img, world.characters[i].x, world.characters[i].y, world.characters[i].w, world.characters[i].h)
	}
}
